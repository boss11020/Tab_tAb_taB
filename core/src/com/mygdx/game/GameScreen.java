package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

public class GameScreen extends ScreenAdapter {
	private TabGame tabGame;
	private Texture tabImg;
	private Texture buttonImg;
	private Texture pressedImg;
	
	private ArrayList<Tab> tabs;
	private Tab tab;
	
	private Button button_1;
	private Button button_2;
	private Button button_3;
	private Button button_4;
	private Button button_5;
	private Button button_6;
	
	boolean pressed_A = false;
	boolean pressed_S = false;
	boolean pressed_D = false;
	boolean pressed_J = false;
	boolean pressed_K = false;
	boolean pressed_L = false;
	
	public static final int SLOT_1 = 1;
	public static final int SLOT_2 = 2;
	public static final int SLOT_3 = 3;
	public static final int SLOT_4 = 4;
	public static final int SLOT_5 = 5;
	public static final int SLOT_6 = 6;

	long timer = TimeUtils.millis();
	int state = 1;
	int num = 1;
	Vector2 pos;
	
	
	
	public GameScreen(TabGame tabGame) {
		this.tabGame = tabGame;
		tabImg = new Texture("tab.png");
    	tabs = new ArrayList<Tab>();
    	buttonImg = new Texture("button.png");
    	pressedImg = new Texture("light.png");
    	setbutton();
    	
    }
	
	@Override
    public void render(float delta) {
		
		control();
		
		if(TimeUtils.millis() - timer > 360){
			updatespawn(num);
			state++;
			timer = TimeUtils.millis();
		}
		
		updatefall(delta);
		Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        SpriteBatch batch = tabGame.batch;
		
        batch.begin();
		
								
		for(Tab t:tabs){
			pos = t.getPosition();
			batch.draw(tabImg, pos.x, pos.y, 100, 50);
		}
		
		pos = button_1.getPosition();
		if(pressed_A == true) {
			batch.draw(pressedImg, pos.x, pos.y, 100, 50);
		}
		else{
			batch.draw(buttonImg, pos.x, pos.y, 100, 50);
		}
		pos = button_2.getPosition();
		if(pressed_S == true) {
			batch.draw(pressedImg, pos.x, pos.y, 100, 50);
		}
		else{
			batch.draw(buttonImg, pos.x, pos.y, 100, 50);
		}
		pos = button_3.getPosition();
		if(pressed_D == true) {
			batch.draw(pressedImg, pos.x, pos.y, 100, 50);
		}
		else{
			batch.draw(buttonImg, pos.x, pos.y, 100, 50);
		}
		pos = button_4.getPosition();
		if(pressed_J == true) {
			batch.draw(pressedImg, pos.x, pos.y, 100, 50);
		}
		else{
			batch.draw(buttonImg, pos.x, pos.y, 100, 50);
		}
		pos = button_5.getPosition();
		if(pressed_K == true) {
			batch.draw(pressedImg, pos.x, pos.y, 100, 50);
		}
		else{
			batch.draw(buttonImg, pos.x, pos.y, 100, 50);
		}
		pos = button_6.getPosition();
		if(pressed_L == true) {
			batch.draw(pressedImg, pos.x, pos.y, 100, 50);
		}
		else{
			batch.draw(buttonImg, pos.x, pos.y, 100, 50);
		}
		batch.end();
	
		resetcontrol();
		
	} 
    	
	private void updatespawn(int num){
		pattern(num);
	}

	private void pattern(int num){
		if(num == 1){
			if(state == 1){
				spawn(1);
				spawn(2);
			}
			if(state == 2){
				spawn(3);
				spawn(2);
			}
			if(state == 3){
				spawn(3);
				spawn(4);
			}
			if(state == 4){
				spawn(5);
				spawn(4);
			}
			if(state == 5){
				spawn(5);
				spawn(6);
			}	
			if(state == 6){
				spawn(1);
				spawn(2);
			}
			if(state == 7){
				spawn(3);
				spawn(2);
			}
			if(state == 8){
				spawn(3);
				spawn(4);
			}
			if(state == 9){
				spawn(5);
				spawn(4);
			}
			if(state == 10){
				spawn(5);
				spawn(6);
			}	
			if(state == 11){
				spawn(1);
				spawn(2);
			}
			if(state == 14){
				spawn(3);
				spawn(2);
			}
			if(state == 17){
				spawn(3);
				spawn(4);
			}
			if(state == 18){
				spawn(5);
				spawn(4);
			}
			if(state == 21){
				spawn(5);
				spawn(6);
			}	
		}
		
	}
	private void updatefall(float delta) {
		for(Tab t:tabs){	
			Vector2 pos = t.getPosition();
			pos.y -= 2;
		}
    }
	
	private void control(){
		if(Gdx.input.isKeyPressed(Keys.A)) {
			pressed_A = true;
		}
		if(Gdx.input.isKeyPressed(Keys.S)) {
			pressed_S = true;
		}
		if(Gdx.input.isKeyPressed(Keys.D)) {
			pressed_D = true;
		}
		if(Gdx.input.isKeyPressed(Keys.J)) {
			pressed_J = true;
		}
		if(Gdx.input.isKeyPressed(Keys.K)) {
			pressed_K = true;
		}
		if(Gdx.input.isKeyPressed(Keys.L)) {
			pressed_L = true;
		}
	}
	
	private void resetcontrol(){
		pressed_A = false;
		pressed_S = false;
		pressed_D = false;
		pressed_J = false;
		pressed_K = false;
		pressed_L = false;
			}
	
	private void spawn(int slot){
		switch(slot) {
        case SLOT_1:
        	tabs.add(new Tab(150,720));
        	break;
        case SLOT_2:
        	tabs.add(new Tab(350,720));
        	break;
        case SLOT_3:
        	tabs.add(new Tab(550,720));
        	break;
        case SLOT_4:
        	tabs.add(new Tab(750,720));
        	break;
        case SLOT_5:
        	tabs.add(new Tab(950,720));
        	break;
        case SLOT_6:
        	tabs.add(new Tab(1150,720));
        	break;
		}
	}
	
	private void setbutton(){
		button_1 = new Button(150,100);
		button_2 = new Button(350,100);
		button_3 = new Button(550,100);
		button_4 = new Button(750,100);
		button_5 = new Button(950,100);
		button_6 = new Button(1150,100);
    }
	
		

}
